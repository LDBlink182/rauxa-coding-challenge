export const environment = {
  production: true,
  swatchesManagerApi: 'http://swatchesmanager-env.eba-jd8mvkng.us-east-2.elasticbeanstalk.com',
  imagesRepository: 'https://swatch-images.s3.us-east-2.amazonaws.com/'
};

import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Swatch } from 'src/app/models/swatch.model';
import { Image } from '../../../models/image.model';
import { UtilsService } from '../../../services/utils.service';
import { SwatchesService } from '../../../services/swatches.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  heading: string;
  content: any;
  action: Subject<any> = new Subject();
  formGroup: FormGroup;
  swatch: Swatch;
  swatchImageOptions = [];

  swatchActiveOptions = [
    { value: 'true', label: 'True' },
    { value: 'false', label: 'False' }
  ];

  constructor(public modalRef: MDBModalRef, private swatchesService: SwatchesService, private utilsService: UtilsService) { }

  ngOnInit() {
    this.swatchImageOptions = this.content.images.map(image => ({ value: { id: image.id }, label: image.name }));
    this.swatch = this.content.swatch;
    if (this.swatch) {
      this.formGroup = new FormGroup({
        swatchActive: new FormControl({ value: `${this.swatch.active}`, disabled: true }, Validators.required),
        swatchName: new FormControl({ value: this.swatch.name, disabled: true }, Validators.required),
        swatchPrice: new FormControl({ value: this.swatch.price, disabled: true }, Validators.required),
        swatchColor: new FormControl({ value: this.swatch.color, disabled: true }, Validators.required),
        swatchDate: new FormControl({ value: this.swatch.date, disabled: true })
      });
    } else {
      this.formGroup = new FormGroup({
        swatchActive: new FormControl('', Validators.required),
        swatchName: new FormControl('', Validators.required),
        swatchPrice: new FormControl('', Validators.required),
        swatchImages: new FormControl('', Validators.required),
        swatchColor: new FormControl('', [Validators.required, Validators.minLength(7), Validators.maxLength(7)])
      });
    }
  }

  get swatchActive() {
    return this.formGroup.get('swatchActive');
  }

  get swatchName() {
    return this.formGroup.get('swatchName');
  }

  get swatchPrice() {
    return this.formGroup.get('swatchPrice');
  }

  get swatchImages() {
    return this.formGroup.get('swatchImages');
  }

  get swatchColor() {
    return this.formGroup.get('swatchColor');
  }

  get swatchDate() {
    return this.formGroup.get('swatchDate');
  }

  getImagePath(image: Image): string {
    return this.utilsService.getImagePath(image);
  }

  onSave() {
    if (this.formGroup.valid) {
      const controls = this.formGroup.controls;
      const swatch = new Swatch();
      swatch.active = JSON.parse(controls.swatchActive.value);
      swatch.name = controls.swatchName.value;
      swatch.price = controls.swatchPrice.value;
      swatch.images = controls.swatchImages.value;
      swatch.color = controls.swatchColor.value;

      this.swatchesService.createSwatch(swatch).subscribe((result: any) => {
        this.action.next({ reload: true });
        this.modalRef.hide();
      });
    } else {
      this.formGroup.markAllAsTouched();
    }
  }
}

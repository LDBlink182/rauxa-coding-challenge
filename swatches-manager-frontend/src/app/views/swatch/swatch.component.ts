import { Component, OnInit } from '@angular/core';
import { Swatch } from '../../models/swatch.model';
import { Image } from '../../models/image.model';
import { MDBModalService, MDBModalRef, ToastService } from 'ng-uikit-pro-standard';
import { ModalComponent } from './modal/modal.component';
import { Observable } from 'rxjs';
import { SwatchesService } from '../../services/swatches.service';
import { UtilsService } from '../../services/utils.service';
import { ImagesService } from '../../services/images.service';

@Component({
  selector: 'app-swatch',
  templateUrl: './swatch.component.html'
})
export class SwatchComponent implements OnInit {

  environment: any;
  modalRef: MDBModalRef;
  notificationSettings: any = { opacity: 0.7 };

  images$: Observable<Image[]>;
  images: Image[];
  swatches$: Observable<Swatch[]>;
  swatches: Swatch[];

  constructor(
    private swatchesService: SwatchesService,
    private imagesService: ImagesService,
    private modalService: MDBModalService,
    private utilsService: UtilsService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.images$ = this.imagesService.getImages();
    this.images$.subscribe((images: Image[]) => {
      this.images = images;
    });
    this.getSwatches();
  }

  getSwatches() {
    this.swatches$ = this.swatchesService.getSwatches();
    this.swatches$.subscribe((swatches: Swatch[]) => {
      this.swatches = swatches;
    });
  }

  getThumbnailPath(image: Image): string {
    return this.utilsService.getThumbnailPath(image);
  }

  openModal(swatch?: Swatch) {
    this.modalRef = this.modalService.show(ModalComponent, {
      backdrop: true,
      keyboard: true,
      focus: true,
      show: false,
      ignoreBackdropClick: false,
      class: 'modal-lg',
      containerClass: '',
      animated: true,
      data: {
        heading: (swatch ? 'View' : 'Create new') + ' Swatch',
        content: { images: this.images, swatch }
      }
    });

    this.modalRef.content.action.subscribe((result: any) => {
      if (result.reload) {
        this.toastService.success('Swatch created!', 'Info', this.notificationSettings);
        this.getSwatches();
      }
    });
  }

  updateSwatch(swatch: Swatch, activate: boolean) {
    this.toastService.success('Updating Swatch...', 'Info', this.notificationSettings);
    this.swatchesService.updateSwatch(swatch.id, activate).subscribe((result: any) => {
      this.toastService.success('Swatch updated!', 'Info', this.notificationSettings);
      this.getSwatches();
    });
  }

  deleteSwatch(swatch: Swatch) {
    this.toastService.success('Deleting Swatch...', 'Info', this.notificationSettings);
    this.swatchesService.deleteSwatch(swatch.id).subscribe((result: any) => {
      this.toastService.success('Swatch deleted!', 'Info', this.notificationSettings);
      this.getSwatches();
    });
  }
}

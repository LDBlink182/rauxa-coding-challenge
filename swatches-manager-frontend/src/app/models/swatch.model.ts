import { Image } from './image.model';
export class Swatch {
    id: number;
    active: boolean;
    name: string;
    price: string;
    images: Image[];
    color: string;
    date: string;
}

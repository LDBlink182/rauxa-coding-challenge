export enum SwatchImageSuffix {
    LARGE_SIZE = '-lg',
    SMALL_SIZE = '-sm'
}

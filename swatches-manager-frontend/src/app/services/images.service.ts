import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Image } from '../models/image.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class ImagesService {

    constructor(private http: HttpClient) { }

    getImages(): Observable<Image[]> {
        return this.http.get<Image[]>(`${environment.swatchesManagerApi}/images`);
    }
}

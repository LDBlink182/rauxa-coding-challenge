import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Swatch } from '../models/swatch.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class SwatchesService {

    constructor(private http: HttpClient) { }

    getSwatches(): Observable<Swatch[]> {
        return this.http.get<Swatch[]>(`${environment.swatchesManagerApi}/swatches`);
    }

    getSwatchById(id: number): Observable<Swatch> {
        return this.http.get<Swatch>(`${environment.swatchesManagerApi}/swatches/${id}`);
    }

    createSwatch(swatch: Swatch): Observable<Swatch> {
        return this.http.post<Swatch>(`${environment.swatchesManagerApi}/swatches`, swatch);
    }

    updateSwatch(id: number, active: boolean): Observable<Swatch> {
        return this.http.put<Swatch>(`${environment.swatchesManagerApi}/swatches/${id}`, { active });
    }

    deleteSwatch(id: number): Observable<any> {
        return this.http.delete<any>(`${environment.swatchesManagerApi}/swatches/${id}`);
    }
}

import { Injectable } from '@angular/core';
import { Image } from '../models/image.model';
import { environment } from '../../environments/environment';
import { SwatchImageSuffix } from '../enums/swatch-image-suffix.enum';

@Injectable()
export class UtilsService {

    constructor() { }

    getImagePath(image: Image): string {
        return environment.imagesRepository + image.name + SwatchImageSuffix.LARGE_SIZE + image.extension;
    }

    getThumbnailPath(image: Image): string {
        return environment.imagesRepository + image.name + SwatchImageSuffix.SMALL_SIZE + image.extension;
    }
}

# Swatches Manager (Frontend)

## Description

Single Page App of a 3 tier web application requested by a coding challenge from [Rauxa](https://rauxa.com/).

## Technologies

- Angular 8

## Installation

```bash
$ npm install
```

## Running the app (Development server)

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
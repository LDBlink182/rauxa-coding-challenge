import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSwatchDto } from './dto/create-swatch.dto';
import { SwatchRepository } from './swatch.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Swatch } from './swatch.entity';
import { DeleteResult } from 'typeorm';
import { UpdateSwatchDto } from './dto/update-swatch.dto';

@Injectable()
export class SwatchesService {
    private findOptions = { relations: ["images"] };

    constructor(
        @InjectRepository(SwatchRepository)
        private swatchRepository: SwatchRepository
    ) { }
    getSwatches(): Promise<Swatch[]> {
        return this.swatchRepository.find({ order: { id: "ASC" }, ...this.findOptions });
    }

    async getSwatchById(id: number): Promise<Swatch> {
        const found = await this.swatchRepository.findOne(id, this.findOptions);

        if (!found) {
            throw new NotFoundException(`Swatch with ID "${id}" not found`);
        }

        return found;
    }

    createSwatch(createSwatchDto: CreateSwatchDto): Promise<Swatch> {
        return this.swatchRepository.createSwatch(createSwatchDto);
    }

    async updateSwatch(id: number, updateSwatchDto: UpdateSwatchDto): Promise<Swatch> {
        const { active } = updateSwatchDto;

        const swatch = await this.getSwatchById(id);
        swatch.active = active;
        await swatch.save();
        return swatch;
    }

    async deleteSwatch(id: number): Promise<DeleteResult> {
        const result = await this.swatchRepository.delete(id);

        if (result.affected === 0) {
            throw new NotFoundException(`Swatch with ID "${id}" not found`);
        }

        return result;
    }
}

import { Module } from '@nestjs/common';
import { SwatchesController } from './swatches.controller';
import { SwatchesService } from './swatches.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SwatchRepository } from './swatch.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([SwatchRepository])
  ],
  controllers: [SwatchesController],
  providers: [SwatchesService]
})
export class SwatchesModule { }

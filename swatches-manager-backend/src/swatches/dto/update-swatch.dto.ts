import { IsNotEmpty, IsBoolean } from 'class-validator';

export class UpdateSwatchDto {
    @IsNotEmpty()
    @IsBoolean()
    active: boolean;
}
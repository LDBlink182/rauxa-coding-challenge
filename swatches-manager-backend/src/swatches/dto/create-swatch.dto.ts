import { IsNotEmpty, IsBoolean, IsArray } from 'class-validator';
import { Image } from '../../images/image.entity';

export class CreateSwatchDto {
    @IsNotEmpty()
    @IsBoolean()
    active: boolean;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    price: string;

    @IsNotEmpty()
    @IsArray()
    images: Image[];

    @IsNotEmpty()
    color: string;
}
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, Unique } from 'typeorm';
import { Image } from '../images/image.entity';

@Entity()
@Unique(['name'])
export class Swatch extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    active: boolean;

    @Column()
    name: string;

    @Column()
    price: string;

    @ManyToMany(type => Image, image => image.swatches)
    @JoinTable()
    images: Image[];

    @Column()
    color: string;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    date: string;
}
import { Repository, EntityRepository } from "typeorm";
import { Swatch } from './swatch.entity';
import { CreateSwatchDto } from './dto/create-swatch.dto';
import { Logger, InternalServerErrorException, ConflictException } from '@nestjs/common';
import { PostgreSQLErrorCodes } from '../utils/postgresql-error-codes.enum';

@EntityRepository(Swatch)
export class SwatchRepository extends Repository<Swatch> {
    private logger = new Logger('SwatchRepository');

    async createSwatch(createSwatchDto: CreateSwatchDto): Promise<Swatch> {
        const { active, name, price, images, color } = createSwatchDto;

        const swatch = new Swatch();
        swatch.active = active;
        swatch.name = name;
        swatch.price = price;
        swatch.images = images;
        swatch.color = color;

        try {
            await swatch.save();
        } catch (error) {
            if (error.code === PostgreSQLErrorCodes.UNIQUE_VIOLATION) {
                throw new ConflictException('Name already exists');
            } else {
                this.logger.error(`Failed to create a swatch. Data: ${JSON.stringify(createSwatchDto)}`, error.stack);
                throw new InternalServerErrorException();
            }
        }
        return swatch;
    }
}
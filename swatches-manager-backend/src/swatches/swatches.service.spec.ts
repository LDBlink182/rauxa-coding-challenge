import { Test } from '@nestjs/testing';
import { SwatchesService } from './swatches.service';
import { SwatchRepository } from './swatch.repository';
import { NotFoundException } from '@nestjs/common';
import { CreateSwatchDto } from './dto/create-swatch.dto';
import { DeleteResult } from 'typeorm';
import { ImageExtension } from '../images/image-extension.enum';
import { Image } from '../images/image.entity';
import { UpdateSwatchDto } from './dto/update-swatch.dto';

const mockImage: Image = {
    id: 1,
    name: 'test',
    extension: ImageExtension.JPG,
    swatches: [],
    hasId: undefined,
    save: undefined,
    remove: undefined,
    reload: undefined
};
const mockCreateSwatchDto: CreateSwatchDto = { active: true, name: 'Test swatch', price: '$10', images: [mockImage], color: '#000000' };
const mockUpdateSwatchDto: UpdateSwatchDto = { active: true };
const mockDeleteResult: DeleteResult = { raw: [], affected: 1 }

const mockSwatchRepository = () => ({
    find: jest.fn(),
    findOne: jest.fn(),
    createSwatch: jest.fn(),
    delete: jest.fn()
});

describe('SwatchesService', () => {
    let swatchesService;
    let swatchRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                SwatchesService,
                { provide: SwatchRepository, useFactory: mockSwatchRepository }
            ]
        }).compile();

        swatchesService = await module.get<SwatchesService>(SwatchesService);
        swatchRepository = await module.get<SwatchRepository>(SwatchRepository);
    });

    describe('getSwatches', () => {
        it('gets all swatches from the repository', async () => {
            swatchRepository.find.mockResolvedValue('someValue');

            expect(swatchRepository.find).not.toHaveBeenCalled();

            const result = await swatchesService.getSwatches();

            expect(swatchRepository.find).toHaveBeenCalled();
            expect(result).toEqual('someValue');
        });
    });

    describe('getSwatchById', () => {
        it('calls swatchRepository.findOne() and successfully retrieve and return the swatch', async () => {
            swatchRepository.findOne.mockResolvedValue(mockCreateSwatchDto);

            const result = await swatchesService.getSwatchById(1);
            expect(result).toEqual(mockCreateSwatchDto);

            expect(swatchRepository.findOne).toHaveBeenCalledWith(1, { relations: ["images"] });
        });

        it('throws an error as swatch is not found', () => {
            swatchRepository.findOne.mockResolvedValue(null);
            expect(swatchesService.getSwatchById(1)).rejects.toThrow(NotFoundException);
        });
    });

    describe('createSwatch', () => {
        it('calls swatchRepository.create() and returns the result', async () => {
            swatchRepository.createSwatch.mockResolvedValue('someSwatch');
            expect(swatchRepository.createSwatch).not.toHaveBeenCalled();
            const result = await swatchesService.createSwatch(mockCreateSwatchDto);
            expect(swatchRepository.createSwatch).toHaveBeenCalledWith(mockCreateSwatchDto);
            expect(result).toEqual('someSwatch');
        });
    });

    describe('updateSwatch', () => {
        it('updates the active property of a swatch', async () => {
            const save = jest.fn().mockResolvedValue(true);

            swatchesService.getSwatchById = jest.fn().mockResolvedValue({
                active: false,
                save
            });

            expect(swatchesService.getSwatchById).not.toHaveBeenCalled();
            expect(save).not.toHaveBeenCalled();
            const result = await swatchesService.updateSwatch(1, mockUpdateSwatchDto);
            expect(swatchesService.getSwatchById).toHaveBeenCalled();
            expect(save).toHaveBeenCalled();
            expect(result.active).toEqual(mockUpdateSwatchDto.active);
        })
    });

    describe('deleteSwatch', () => {
        it('calls swatchRepository.delete() and successfully retrieve and return the delete result', async () => {
            swatchRepository.delete.mockResolvedValue(mockDeleteResult);

            const result = await swatchesService.deleteSwatch(1);
            expect(result).toEqual(mockDeleteResult);

            expect(swatchRepository.delete).toHaveBeenCalledWith(1);
        });

        it('throws an error as swatch is not found', () => {
            swatchRepository.delete.mockResolvedValue({ raw: [], affected: 0 });
            expect(swatchesService.deleteSwatch(1)).rejects.toThrow(NotFoundException);
        });
    });
});
import { Controller, Get, Post, Body, Param, Delete, Put, UsePipes, ValidationPipe, ParseIntPipe, Logger } from '@nestjs/common';
import { SwatchesService } from './swatches.service';
import { CreateSwatchDto } from './dto/create-swatch.dto';
import { Swatch } from './swatch.entity';
import { DeleteResult } from 'typeorm';
import { UpdateSwatchDto } from './dto/update-swatch.dto';

@Controller('swatches')
export class SwatchesController {
    private logger = new Logger('SwatchesController');
    constructor(private swatchesService: SwatchesService) { }

    @Get()
    getSwatches(): Promise<Swatch[]> {
        this.logger.verbose(`Retrieving all swatches.`);
        return this.swatchesService.getSwatches();
    }

    @Get('/:id')
    getSwatchById(@Param('id', ParseIntPipe) id: number): Promise<Swatch> {
        return this.swatchesService.getSwatchById(id);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createSwatch(@Body() createSwatchDto: CreateSwatchDto): Promise<Swatch> {
        this.logger.verbose(`Creating a new swatch. Data: ${JSON.stringify(createSwatchDto)}`);
        return this.swatchesService.createSwatch(createSwatchDto);
    }

    @Put('/:id')
    updateSwatch(
        @Param('id', ParseIntPipe) id: number,
        @Body() updateSwatchDto: UpdateSwatchDto
    ): Promise<Swatch> {
        return this.swatchesService.updateSwatch(id, updateSwatchDto);
    }

    @Delete('/:id')
    deleteSwatch(@Param('id', ParseIntPipe) id: number): Promise<DeleteResult> {
        return this.swatchesService.deleteSwatch(id);
    }
}

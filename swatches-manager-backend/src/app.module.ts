import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { ImagesModule } from './images/images.module';
import { SwatchesModule } from './swatches/swatches.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    ImagesModule,
    SwatchesModule
  ],
})
export class AppModule { }

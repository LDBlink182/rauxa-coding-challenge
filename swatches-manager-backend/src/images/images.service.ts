import { Injectable, NotFoundException } from '@nestjs/common';
import { ImageExtension } from './image-extension.enum';
import { CreateImageDto } from './dto/create-image.dto';
import { GetImagesFilterDto } from './dto/get-images-filter.dto';
import { ImageRepository } from './image.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Image } from './image.entity';
import { DeleteResult } from 'typeorm';

@Injectable()
export class ImagesService {
    constructor(
        @InjectRepository(ImageRepository)
        private imageRepository: ImageRepository
    ) { }
    getImages(filterDto: GetImagesFilterDto): Promise<Image[]> {
        return this.imageRepository.getImages(filterDto);
    }

    async getImageById(id: number): Promise<Image> {
        const found = await this.imageRepository.findOne(id);

        if (!found) {
            throw new NotFoundException(`Image with ID "${id}" not found`);
        }

        return found;
    }

    createImage(createImageDto: CreateImageDto): Promise<Image> {
        return this.imageRepository.createImage(createImageDto);
    }

    async updateImageExtension(id: number, extension: ImageExtension): Promise<Image> {
        const image = await this.getImageById(id);
        image.extension = extension;
        await image.save();
        return image;
    }

    async deleteImage(id: number): Promise<DeleteResult> {
        const result = await this.imageRepository.delete(id);

        if (result.affected === 0) {
            throw new NotFoundException(`Image with ID "${id}" not found`);
        }

        return result;
    }
}

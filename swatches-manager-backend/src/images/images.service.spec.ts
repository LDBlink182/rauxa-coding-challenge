import { Test } from '@nestjs/testing';
import { ImagesService } from './images.service';
import { ImageRepository } from './image.repository';
import { GetImagesFilterDto } from './dto/get-images-filter.dto';
import { ImageExtension } from './image-extension.enum';
import { NotFoundException } from '@nestjs/common';
import { CreateImageDto } from './dto/create-image.dto';
import { DeleteResult } from 'typeorm';

const mockImageDto: CreateImageDto = { name: 'Test image', extension: ImageExtension.JPG };
const mockDeleteResult: DeleteResult = { raw: [], affected: 1 }

const mockImageRepository = () => ({
    getImages: jest.fn(),
    findOne: jest.fn(),
    createImage: jest.fn(),
    delete: jest.fn()
});

describe('ImagesService', () => {
    let imagesService;
    let imageRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                ImagesService,
                { provide: ImageRepository, useFactory: mockImageRepository }
            ]
        }).compile();

        imagesService = await module.get<ImagesService>(ImagesService);
        imageRepository = await module.get<ImageRepository>(ImageRepository);
    });

    describe('getImages', () => {
        it('gets all images from the repository', async () => {
            imageRepository.getImages.mockResolvedValue('someValue');

            expect(imageRepository.getImages).not.toHaveBeenCalled();

            const filters: GetImagesFilterDto = { extension: ImageExtension.JPG, search: 'Some search query' };
            const result = await imagesService.getImages(filters);

            expect(imageRepository.getImages).toHaveBeenCalled();
            expect(result).toEqual('someValue');
        });
    });

    describe('getImageById', () => {
        it('calls imageRepository.findOne() and successfully retrieve and return the image', async () => {
            imageRepository.findOne.mockResolvedValue(mockImageDto);

            const result = await imagesService.getImageById(1);
            expect(result).toEqual(mockImageDto);

            expect(imageRepository.findOne).toHaveBeenCalledWith(1);
        });

        it('throws an error as image is not found', () => {
            imageRepository.findOne.mockResolvedValue(null);
            expect(imagesService.getImageById(1)).rejects.toThrow(NotFoundException);
        });
    });

    describe('createImage', () => {
        it('calls imageRepository.create() and returns the result', async () => {
            imageRepository.createImage.mockResolvedValue('someImage');
            expect(imageRepository.createImage).not.toHaveBeenCalled();
            const result = await imagesService.createImage(mockImageDto);
            expect(imageRepository.createImage).toHaveBeenCalledWith(mockImageDto);
            expect(result).toEqual('someImage');
        });
    });

    describe('updateImageExtension', () => {
        it('updates a image extension', async () => {
            const save = jest.fn().mockResolvedValue(true);

            imagesService.getImageById = jest.fn().mockResolvedValue({
                extension: ImageExtension.JPG,
                save
            });

            expect(imagesService.getImageById).not.toHaveBeenCalled();
            expect(save).not.toHaveBeenCalled();
            const result = await imagesService.updateImageExtension(1, ImageExtension.JPG);
            expect(imagesService.getImageById).toHaveBeenCalled();
            expect(save).toHaveBeenCalled();
            expect(result.extension).toEqual(ImageExtension.JPG);
        })
    });

    describe('deleteImage', () => {
        it('calls imageRepository.delete() and successfully retrieve and return the delete result', async () => {
            imageRepository.delete.mockResolvedValue(mockDeleteResult);

            const result = await imagesService.deleteImage(1);
            expect(result).toEqual(mockDeleteResult);

            expect(imageRepository.delete).toHaveBeenCalledWith(1);
        });

        it('throws an error as image is not found', () => {
            imageRepository.delete.mockResolvedValue({ raw: [], affected: 0 });
            expect(imagesService.deleteImage(1)).rejects.toThrow(NotFoundException);
        });
    });
});
import { ImageExtension } from '../image-extension.enum';
import { IsOptional, IsIn, IsNotEmpty } from 'class-validator';

export class GetImagesFilterDto {
    @IsOptional()
    @IsIn([ImageExtension.JPG])
    extension: ImageExtension;

    @IsOptional()
    @IsNotEmpty()
    search: string;
}
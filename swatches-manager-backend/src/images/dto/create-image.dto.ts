import { IsNotEmpty } from 'class-validator';
import { ImageExtension } from '../image-extension.enum';

export class CreateImageDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    extension: ImageExtension;
}
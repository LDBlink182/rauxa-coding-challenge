import { Controller, Get, Post, Body, Param, Delete, Patch, Query, UsePipes, ValidationPipe, ParseIntPipe, Logger } from '@nestjs/common';
import { ImagesService } from './images.service';
import { ImageExtension } from './image-extension.enum';
import { CreateImageDto } from './dto/create-image.dto';
import { GetImagesFilterDto } from './dto/get-images-filter.dto';
import { ImageExtensionValidationPipe } from './pipes/image-extension-validation.pipe';
import { Image } from './image.entity';
import { DeleteResult } from 'typeorm';

@Controller('images')
export class ImagesController {
    private logger = new Logger('ImagesController');
    constructor(private imagesService: ImagesService) { }

    @Get()
    getImages(@Query(ValidationPipe) filterDto: GetImagesFilterDto): Promise<Image[]> {
        this.logger.verbose(`Retrieving all images. Filters: ${JSON.stringify(filterDto)}`);
        return this.imagesService.getImages(filterDto);
    }

    @Get('/:id')
    getImageById(@Param('id', ParseIntPipe) id: number): Promise<Image> {
        return this.imagesService.getImageById(id);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createImage(@Body() createImageDto: CreateImageDto): Promise<Image> {
        this.logger.verbose(`Creating a new image. Data: ${JSON.stringify(createImageDto)}`);
        return this.imagesService.createImage(createImageDto);
    }

    @Patch('/:id/extension')
    updateImageExtension(
        @Param('id', ParseIntPipe) id: number,
        @Body('extension', ImageExtensionValidationPipe) extension: ImageExtension
    ): Promise<Image> {
        return this.imagesService.updateImageExtension(id, extension);
    }

    @Delete('/:id')
    deleteImage(@Param('id', ParseIntPipe) id: number): Promise<DeleteResult> {
        return this.imagesService.deleteImage(id);
    }
}

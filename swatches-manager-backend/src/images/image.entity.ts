import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToMany, Unique } from 'typeorm';
import { ImageExtension } from './image-extension.enum';
import { Swatch } from '../swatches/swatch.entity';

@Entity()
@Unique(['name'])
export class Image extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    extension: ImageExtension;

    @ManyToMany(type => Swatch, swatch => swatch.images)
    swatches: Swatch[];
}
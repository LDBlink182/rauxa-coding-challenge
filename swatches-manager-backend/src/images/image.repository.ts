import { Repository, EntityRepository } from "typeorm";
import { Image } from './image.entity';
import { CreateImageDto } from './dto/create-image.dto';
import { GetImagesFilterDto } from './dto/get-images-filter.dto';
import { Logger, InternalServerErrorException, ConflictException } from '@nestjs/common';
import { PostgreSQLErrorCodes } from '../utils/postgresql-error-codes.enum';

@EntityRepository(Image)
export class ImageRepository extends Repository<Image> {
    private logger = new Logger('ImageRepository');

    async getImages(filterDto: GetImagesFilterDto): Promise<Image[]> {
        const { extension, search } = filterDto;
        const query = this.createQueryBuilder('image');

        if (extension) {
            query.where('image.extension = :extension', { extension });
        }

        if (search) {
            query.andWhere('image.name LIKE :search', { search: `%${search}%` });
        }

        try {
            const images = await query.getMany();
            return images;
        } catch (error) {
            this.logger.error(`Failed to get images. Filters: ${JSON.stringify(filterDto)}`, error.stack);
            throw new InternalServerErrorException();
        }
    }

    async createImage(createImageDto: CreateImageDto): Promise<Image> {
        const { name, extension } = createImageDto;

        const image = new Image();
        image.name = name;
        image.extension = extension;

        try {
            await image.save();
        } catch (error) {
            if (error.code === PostgreSQLErrorCodes.UNIQUE_VIOLATION) {
                throw new ConflictException('Name already exists');
            } else {
                this.logger.error(`Failed to create a image. Data: ${JSON.stringify(createImageDto)}`, error.stack);
                throw new InternalServerErrorException();
            }
        }
        return image;
    }
}
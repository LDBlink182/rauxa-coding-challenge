import { PipeTransform, BadRequestException } from "@nestjs/common";
import { ImageExtension } from '../image-extension.enum';

export class ImageExtensionValidationPipe implements PipeTransform {
    readonly allowedExtensions = [
        ImageExtension.JPG
    ];

    transform(value: any) {
        value = value.toLowerCase();

        if (!this.isExtensionValid(value)) {
            throw new BadRequestException(`"${value}" is an invalid extension`);
        }

        return value;
    }

    private isExtensionValid(extension: any) {
        const idx = this.allowedExtensions.indexOf(extension);
        return idx !== -1;
    }
}
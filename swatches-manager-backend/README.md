# Swatches Manager (Backend)

## Description

Backend service of a 3 tier web application requested by a coding challenge from [Rauxa](https://rauxa.com/).

## Technologies

- Node.js
- NestJS

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# watch mode
$ npm run test:watch
```

## Additional notes

This application runs on port 3000.
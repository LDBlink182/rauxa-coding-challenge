# Swatches Manager

## Description of the application

Swatches Manager is a 3 tier web application requested by a coding challenge from [Rauxa](https://rauxa.com/). This application lets you manage swatches, including features such as view the details for an individual swatch and create new swatches.

## List of features

- Create a new Swatch using a single image;
- Create a new Swatch using multiple images;
- List existing Swatches;
- View the details of a selected Swatch;
- Toggle the active status of a Swatch directly from the list of Swatches;
- Delete a Swatch directly from the list of Swatches.

## Commands to run the application

The commands and instructions to run the application (frontend and backend) are described in the README.md file of each project, located under their respective folders in this repository.

## Hosted Application

Go to [this](http://swatches-manager-frontend.s3-website.us-east-2.amazonaws.com) address to access the hosted application.

## Tests

For testing, some unit tests were written only in the backend application, due to the limited time available. These tests cover the service functionalities created for the application, therefore each `Service` file has its own `Spec` variant.

## Main Technologies

- Angular 8
- Node.js
- NestJS
- PostgreSQL
- Amazon S3, EC2, RDS, Elastic Beanstalk

## Technical choices

`Node.js` and `Amazon Web Services` in general were explicitly requested in the challenge description. `NestJS` and `PostgreSQL` were chosen due to their efficiency and reliability. `Angular` was chosen due to the lack of knowledge in `VueJS`, being a good second choice to build Single Page Apps (also requested in the challenge).

## Improvements

Overall, the project achieved the requirements of the challenge (including the bonus), however, with additional time, extra work must be added to this project to reach its full potential, such as:

- Full coverage of comments in the source-code (Backend & Frontend);
- Full coverage of unit tests (Backend & Frontend);
- UI Adjustments (Frontend).